<?php
/**
 * Created by PhpStorm.
 * User: Damian
 * Date: 2015-04-19
 * Time: 22:43
 */

require_once('autoLoad.php');

$tpl = new MiniTemplator();
$tpl->readTemplateFromFile('theme/index.html');
User::autoLogin();
if(User::isLogged() == false) header("Location: login.php");

if(isset($_GET['mod']))$mod = $_GET['mod']; else $mod = '';
####################################################
## ustawienia konta
####################################################
if($mod == "editUser")
{
    if(User::can(DbUsers::getName(),User::P_UPDATE))
    {
        $user = DbUsers::getUserById(($_GET['id']));
        $tpl->setVariable('ID',$user['id']);
        $tpl->setVariable('LOGIN',$user['name']);
        $tpl->setVariable('EMAIL',$user['e_mail']);
        $tpl->addBlock('BOX_EDIT_ACCOUNT');
    }
    else
    {
        tpl_showPermissionError($tpl,DbUsers::getName(),User::P_UPDATE);
    }
}
if($mod == "editusertodb") {
    if(User::can(DbUsers::getName(),User::P_UPDATE)) {
        DbUsers::editUserNameAndEmailById($_POST['login'],$_POST['email'],$_POST['id']);

        if(!empty($_POST['new_pass']))
        {
            DbUsers::editUserPassword($_POST['login'],$_POST['new_pass']);
        }

        header('Location: users.php?mod=list');
    }
    else tpl_showPermissionError($tpl,DbUsers::getName(),User::P_UPDATE);
}
####################################################
## Usunięcie konta
####################################################
elseif($mod == "deleteUser")
{
    if(User::can(DbUsers::getName(),User::P_DELETE))
    {
        DbUsers::deleteAccountById($_GET['id']);
        header('Location: users.php?mod=list');
    }
    else
    {
        tpl_showPermissionError($tpl,DbUsers::getName(),User::P_DELETE);
    }
}
####################################################
## Zmien aktualną rolę
####################################################
elseif($mod == 'changerole') {
    User::changeRole($_POST['role']);
    header('Location: index.php');
}
####################################################
## Lista użytkowników
####################################################
elseif($mod == 'list') {
    if(User::can(DbUsers::getName(),User::P_SELECT))
    {
        $canDeleteUser = User::can(DbUsers::getName(),User::P_DELETE);
        $canEditUser = User::can(DbUsers::getName(),User::P_UPDATE);
        $canRegisterUser  = User::can(DbUsers::getName(),User::P_UPDATE);
        $tpl->setVariable('EDYTUJ','');
        $tpl->setVariable('USUN','');
        $tpl->setVariable('OPCJE','');

        $users = DbUsers::getUsers();

        foreach($users as $user)
        {
            $tpl->setVariable('LOGIN',$user['name']);
            if($canEditUser){$tpl->setVariable('EDYTUJ','<a href="users.php?mod=editUser&id='.$user['id'].'">Edytuj</a>');}
            if($canDeleteUser){$tpl->setVariable('USUN','<a href="users.php?mod=deleteUser&id='.$user['id'].'" onclick="return confirm(\'Czy na pewno?\')">Usun użytkownika</a>');}
            if($canRegisterUser)
            {
                if($user['accountActive'] == '0')
                {
                    $tpl->setVariable('OPCJE','<a href="users.php?mod=activation&set=1&username='.$user['name'].'">Aktywuj konto</a>');
                }
                else
                {
                    $tpl->setVariable('OPCJE','<a href="users.php?mod=activation&set=0&username='.$user['name'].'">Dezakywuj konto</a>');
                }

            }

            $tpl->setVariable('JS_CAN_EDIT',$canEditUser ? 'true' : 'false');
            $tpl->setVariable('JS_CAN_DELETE',$canDeleteUser ? 'true' : 'false');

            if($user['name'] == User::getUserName())
            {
                $tpl->setVariable('USUN','-');
                $tpl->setVariable('OPCJE','-');
            }

            $tpl->addBlock('BOX_LISTUSER_USER');
        }
        $tpl->addBlock('BOX_LISTUSER');

    }
    else tpl_showPermissionError($tpl,DbUsers::getName(),User::P_SELECT);
}
####################################################
## aktywacja/dezaktywacja użytkownika
####################################################
elseif($mod == 'activation') {
    if(User::can(DbUsers::getName(),User::P_UPDATE))
    {
        if($_GET['set']==1)
        {
            DbUsers::activateAccount($_GET['username']);
            header('Location: users.php?mod=list');
        }
        elseif($_GET['set']==0)
        {
            DbUsers::deactivateAccount($_GET['username']);
            header('Location: users.php?mod=list');
        }
    }
    else tpl_showPermissionError($tpl,DbUsers::getName(),User::P_UPDATE);
}
####################################################
## Dodawanie nowego użytkownika
####################################################
elseif($mod == 'adduser') {
    if(User::can(DbUsers::getName(),User::P_INSERT))
    {
        $tpl->addBlock('BOX_REGISTER');
    }
    else tpl_showPermissionError($tpl,DbUsers::getName(),User::P_INSERT);
}


tpl_showLeftMenu($tpl);
tpl_showHeaderMenu($tpl);

$tpl->generateOutput();

