<?php
/**
 * Created by PhpStorm.
 * User: Damian
 * Date: 2015-05-24
 * Time: 02:00
 */

require_once('autoLoad.php');

$tpl = new MiniTemplator();
$tpl->readTemplateFromFile('theme/index.html');
User::autoLogin();

if(!User::isLogged())
{
    header('Location: login.php');
}
tpl_showLeftMenu($tpl);
tpl_showHeaderMenu($tpl);

$mod = isset($_GET['mod']) ? $_GET['mod'] : '';
##############################################
## Insert
##############################################
if($mod == 'add')
{
    if(User::can(DbFilm::getName(),User::P_INSERT))
    {
        $tpl->setVariable('MOD','addtodb');
        $tpl->setVariable('NAME_FUNCTION','Dodaj typ biletu');
        $tpl->addBlock('BOX_TYPYBILETOW_ADDEDIT');
    }
}
elseif($mod == 'addtodb')
{
    if(User::can(DbFilm::getName(),User::P_INSERT))
    {
        $params['nazwa'] = $_POST['nazwa'];
        $params['cena'] = $_POST['cena'];
        $params['status'] = $_POST['status'];
        DbTypyBiletow::insert($params);
        header('Location: typybiletow.php');
    }
}
##############################################
## Update
##############################################
elseif($mod == 'edit')
{
    if(User::can(DbTypyBiletow::getName(),User::P_UPDATE))
    {
        $element = DbTypyBiletow::selectById($_GET['id']);
        $tpl->setVariablesToUpper($element, true);
        $tpl->setVariable('CHECKED_STATUS_'.$element['status'],'checked');
        $tpl->setVariable('MOD','edittodb');
        $tpl->setVariable('NAME_FUNCTION','Edytuj typ biletu');
        $tpl->addBlock('BOX_TYPYBILETOW_ADDEDIT');
    }
}
elseif($mod == 'edittodb')
{
    if(User::can(DbTypyBiletow::getName(),User::P_UPDATE))
    {
        $params['nazwa'] = $_POST['nazwa'];
        $params['cena'] = $_POST['cena'];
        $params['status'] = $_POST['status'];
        $params['id'] = $_POST['id'];
        DbTypyBiletow::updateById($params['id'],$params);
        header('Location: typybiletow.php');
    }
}
##############################################
## Delete
##############################################
elseif($mod == 'delete')
{
    if(User::can(DbTypyBiletow::getName(),User::P_DELETE))
    {
        DbTypyBiletow::deleteById($_GET['id']);
        header('Location: typybiletow.php');
    }
}
##############################################
## Select
##############################################
else
{
    if(User::can(DbTypyBiletow::getName(),User::P_SELECT))
    {
        $canUpdateFilm = User::can(DbTypyBiletow::getName(),User::P_UPDATE);
        $canDeleteFilm = User::can(DbTypyBiletow::getName(),User::P_DELETE);

        $typy = DbTypyBiletow::selectAll();
        foreach($typy as $element)
        {
            $tpl->setVariablesToUpper($element,true);
            $tpl->setVariable('STATUS_DLUGI',DbTypyBiletow::getStatusDescription($element['status']));
            $tpl->setVariable('EDYTUJ','');
            $tpl->setVariable('USUN','');
            $tpl->setVariable('JS_CAN_EDIT',$canUpdateFilm ? 'true' : 'false');
            $tpl->setVariable('JS_CAN_DELETE',$canDeleteFilm ? 'true' : 'false');

            if($canUpdateFilm)
                $tpl->setVariable('EDYTUJ','<a href="typybiletow.php?mod=edit&id='.$element['id'].'">Edytuj</a>');
            if($canDeleteFilm)
                $tpl->setVariable('USUN','<a href="typybiletow.php?mod=delete&id='.$element['id'].'" onclick="return confirm(\'Czy na pewno?\')">Usun</a>');

            $tpl->addBlock('BOX_TYPYBILETOW_ITEM');
        }
        $tpl->addBlock('BOX_TYPYBILETOW');
    }
}
$tpl->generateOutput();