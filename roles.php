<?php
/**
 * Created by PhpStorm.
 * User: Damian
 * Date: 2015-04-19
 * Time: 17:12
 */

require_once('autoLoad.php');

$tpl = new MiniTemplator();
$tpl->readTemplateFromFile('theme/index.html');
User::autoLogin();
if(User::isLogged() == false) header("Location: login.php");

if(isset($_GET['mod']))$mod = $_GET['mod']; else $mod = '';
####################################################
## Lista ról
####################################################
if($mod == 'list' or $mod == '')
{
    if(User::can(DbRoles::getName(),User::P_SELECT))
    {
        $canEditRole = User::can(DbRoles::getName(),User::P_UPDATE);
        $canDeleteRole = User::can(DbRoles::getName(),User::P_DELETE);

        $roles = DbRoles::getAllRoles();
        foreach($roles as $role)
        {
            $tpl->setVariable('NAZWA',$role['name']);
            if($canEditRole)
            {
                $tpl->setVariable('EDYTUJ','<a href="roles.php?mod=editrole&id='.$role['id'].'">Edytuj</a>');
            }
            if($canDeleteRole)
            {
                $tpl->setVariable('USUN','<a href="roles.php?mod=deleterole&id='.$role['id'].'" onclick="return confirm(\'Czy na pewno?\')">Usuń</a>');
            }
            $tpl->setVariable('JS_CAN_EDIT',$canEditRole ? 'true' : 'false');
            $tpl->setVariable('JS_CAN_DELETE',$canDeleteRole ? 'true' : 'false');
            $tpl->addBlock('BOX_ROLES_ROLE');
        }
        $tpl->addBlock('BOX_ROLES');
    }
    else
    {
        tpl_showPermissionError($tpl,DbRoles::getName(),User::P_SELECT);
    }
}
####################################################
## Dodaj rolę
####################################################
elseif($mod == 'addrole') {
    if(User::can(DbRoles::getName(),User::P_INSERT)) {
        $tables = DbRoles::getTables();
        sort($tables);
        foreach($tables as $table)
        {
            $tpl->setVariable('TABLE',substr($table,2));
            $tpl->addBlock('BOX_ADD_ROLE_TABLE');
        }

        $tpl->setVariable('NAME_FUNCTION','Dodaj nową rolę');
        $tpl->setVariable('MOD','addroletodb');
        $tpl->addBlock('BOX_ADD_ROLE');
    }
    else
    {
        tpl_showPermissionError($tpl,DbRoles::getName(),User::P_INSERT);
    }
}
elseif($mod == 'addroletodb'){
    if(User::can(DbRoles::getName(),User::P_INSERT)) {

        $params['name'] = $_POST['role_name'];
        $perms = $_POST['perm'];
        foreach ($perms as $perm)
        {
            //analiza pola
            $p = explode('-',$perm);
            $table = 't_'.$p[0];
            $privilege = $p[1];

            //stworzenie parametru, jezeli nie istnieje
            if(!isset($params[$table]))
            {
                $params[$table] = '0000';
            }

            //ustawienie przywileju
            switch($privilege)
            {
                case 'select': $params[$table] = '1'.substr($params[$table],1); break;
                case 'insert': $params[$table][1] = '1'; break;
                case 'update': $params[$table][2] = '1'; break;
                case 'delete': $params[$table][3] = '1'; break;
                default: break;
            }
        }

        if(empty($params['name']))
        {
            tpl_showError($tpl, 'Musisz podać nazwę roli!');
        }
        else
        {
            DbRoles::createRole($params);
            header('Location: roles.php');
        }
    }
    else
    {
        tpl_showPermissionError($tpl,DbRoles::getName(),User::P_INSERT);
    }
}
####################################################
## Usuń rolę
####################################################
elseif($mod == 'deleterole' and $_GET['id']>0) {
    if(User::can(DbRoles::getName(),User::P_DELETE))
    {
        DbRoles::deleteRoleById($_GET['id']);
        header('Location: roles.php?mod=list');
    }
    else
    {
        tpl_showPermissionError($tpl,DbRoles::getName(),User::P_DELETE);
    }
}
####################################################
## Edytuj rolę
####################################################
elseif($mod == 'editrole' and $_GET['id']>0) {
    if(User::can(DbRoles::getName(),User::P_UPDATE))
    {
        $role = DbRoles::getRoleByID($_GET['id']);
        if(empty($role['name']))
        {
            tpl_showError($tpl, 'Nie ma takiej roli!');
        }
        else
        {
            $tpl->setVariable('ROLE_NAME',$role['name']);
            $tpl->setVariable('ROLE_ID',$role['id']);

            $tables = DbRoles::getTables();
            sort($tables);
            $checked_types = array('CHECKED_SELECT', 'CHECKED_INSERT', 'CHECKED_UPDATE', 'CHECKED_DELETE');
            foreach($tables as $table)
            {
                $tpl->setVariable('TABLE',substr($table,2));

                for($i = 0; $i<count($checked_types); $i++)
                {
                    if(substr($role[$table],$i,1) == User::ACCESS_GRANTED)
                    {
                        $tpl->setVariable($checked_types[$i],'checked');
                    }
                    else
                    {
                        $tpl->setVariable($checked_types[$i],'');
                    }
                }
                $tpl->addBlock('BOX_ADD_ROLE_TABLE');
            }
            $tpl->setVariable('NAME_FUNCTION','Edytuj rolę');
            $tpl->setVariable('MOD','editroletodb');
            $tpl->addBlock('BOX_ADD_ROLE');
        }
    }
    else
    {
        tpl_showPermissionError($tpl,DbRoles::getName(),User::P_UPDATE);
    }
}
elseif($mod == 'editroletodb') {
    if (User::can(DbRoles::getName(),User::P_UPDATE)) {
        $id = $_POST['role_id'];
        $params['name'] = $_POST['role_name'];
        $perms = $_POST['perm'];

        $tables = DbRoles::getTables();
        foreach($tables as $table)
        {
            $params[$table] = '0000';
        }

        foreach ($perms as $perm)
        {
            //analiza pola
            $p = explode('-',$perm);
            $table = 't_'.$p[0];
            $privilege = $p[1];

            //stworzenie parametru, jezeli nie istnieje
            if(!isset($params[$table]))
            {
                $params[$table] = '0000';
            }

            //ustawienie przywileju
            switch($privilege)
            {
                case 'select': $params[$table] = '1'.substr($params[$table],1); break;
                case 'insert': $params[$table][1] = '1'; break;
                case 'update': $params[$table][2] = '1'; break;
                case 'delete': $params[$table][3] = '1'; break;
                default: break;
            }
        }
        if(empty($params['name']))
        {
            tpl_showError($tpl, 'Musisz podać nazwę roli!');
        }
        elseif(empty($id))
        {
            tpl_showError($tpl, 'Brak identyfikatora roli!');
        }
        else
        {
            DbRoles::editRoleById($id,$params);
            header('Location: roles.php');
        }
    }
    else
    {
        tpl_showPermissionError($tpl,DbRoles::getName(),User::P_UPDATE);
    }
}
####################################################
## Przydzielanie ról
####################################################
elseif($mod == 'userrole'){
    $canSelectUsers = User::can(DbUsers::getName(),User::P_SELECT);
    $canSelectRoles = User::can(DbRoles::getName(),User::P_SELECT);
    $canInsertMembership = User::can(DbMembership::getName(),User::P_INSERT);
    if($canInsertMembership)
    {
        $users = $canSelectUsers ? DbUsers::getUsers() : array();
        $roles = $canSelectRoles ? DbRoles::getAllRoles() : array();
        $tpl->setVariable('JS_AVAILABLE_USERS',json_encode(array_column($users,'name')));
        $tpl->setVariable('JS_AVAILABLE_ROLES',json_encode(array_column($roles,'name')));
        $tpl->addBlock('BOX_ADD_USERROLE');
    }
    if(User::can(DbMembership::getName(),User::P_DELETE) and User::can(DbUsers::getName(),User::P_SELECT))
    {
        $users = DbUsers::getUsers();
        foreach($users as $user)
        {
            $tpl->setVariable('LOGIN',$user['name']);
            $roles = DbMembership::getUserRoles($user['id']);
            foreach($roles as $role)
            {
                $tpl->setVariable('NAZWA_ROLI', $role['name']);
                $tpl->setVariable('USER_ID', $user['id']);
                $tpl->setVariable('ROLE_ID',$role['id']);
                $tpl->addBlock('BOX_LISTUSERROLES_USER_ROLE');
            }

            $tpl->addBlock('BOX_LISTUSERROLES_USER');
        }
        $tpl->addBlock('BOX_LISTUSERROLES');
    }
}
####################################################
## Kasowanie roli użytkownika
####################################################
elseif($mod == 'deleteUserRole') {
    if (User::can(DbMembership::getName(),User::P_DELETE)) {
        DbMembership::deleteUserRole($_GET['user_id'],$_GET['role_id']);
        header('Location: roles.php?mod=userrole');
    } else tpl_showPermissionError($tpl,DbMembership::getName(),User::P_DELETE);
}
####################################################
## Dodawanie roli użytkownika
####################################################
elseif($mod == 'adduserroletodb') {
    if (User::can(DbMembership::getName(),User::P_INSERT)) {
        $success = DbMembership::addUserRoleByNames($_POST['user_name'],$_POST['role_name']);
        if($success)
        {
            header('Location: roles.php?mod=userrole');
        }
        else
        {
            header('Location: roles.php?mod=userrole&info_error=Wystąpił błąd');
        }

    }
    else tpl_showPermissionError($tpl,DbMembership::getName(),User::P_INSERT);
}

tpl_showLeftMenu($tpl);
tpl_showHeaderMenu($tpl);

$tpl->generateOutput();

