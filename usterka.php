<?php
/**
 * Created by PhpStorm.
 * User: Damian
 * Date: 2015-05-24
 * Time: 02:00
 */

require_once('autoLoad.php');

$tpl = new MiniTemplator();
$tpl->readTemplateFromFile('theme/index.html');
User::autoLogin();

if(!User::isLogged())
{
    header('Location: login.php');
}
tpl_showLeftMenu($tpl);
tpl_showHeaderMenu($tpl);

$mod = isset($_GET['mod']) ? $_GET['mod'] : '';
##############################################
## Insert
##############################################
if($mod == 'add')
{
    if(User::can(DbUsterka::getName(),User::P_INSERT))
    {
        $tpl->setVariable('CHECKED_WAZNOSC_2','checked');
        $tpl->setVariable('CHECKED_STATUS_N','checked');
        $tpl->setVariable('NAME_FUNCTION','Dodaj usterkę');
        $tpl->setVariable('MOD','addtodb');
        $tpl->addBlock('BOX_USTERKA_ADDEDIT');
    }
}
elseif($mod == 'addtodb')
{
    if(User::can(DbUsterka::getName(),User::P_INSERT))
    {
        $params['opis'] = $_POST['opis'];
        $params['status'] = $_POST['status'];
        $params['waznosc'] = $_POST['waznosc'];
        $params['user_id_zglaszajacy'] = User::getId();

        DbUsterka::insert($params);
        header('Location: usterka.php');
    }
}
##############################################
## Update
##############################################
elseif($mod == 'edit')
{
    if(User::can(DbUsterka::getName(),User::P_UPDATE))
    {
        $element = DbUsterka::selectById($_GET['id']);
        $tpl->setVariablesToUpper($element, true);
        $tpl->setVariable('CHECKED_WAZNOSC_'.$element['waznosc'],'checked');
        $tpl->setVariable('CHECKED_STATUS_'.$element['status'],'checked');
        $tpl->setVariable('MOD','edittodb');
        $tpl->setVariable('NAME_FUNCTION','Edytuj usterkę');
        $tpl->addBlock('BOX_USTERKA_ADDEDIT');
    }
}
elseif($mod == 'edittodb')
{
    if(User::can(DbUsterka::getName(),User::P_UPDATE))
    {
        $params['id'] = $_POST['id'];
        $params['opis'] = $_POST['opis'];
        $params['status'] = $_POST['status'];
        $params['waznosc'] = $_POST['waznosc'];
        $params['user_id_modyfikujacy'] = User::getId();

        DbUsterka::updateById($params['id'],$params);
        header('Location: usterka.php');
    }
}
##############################################
## Delete
##############################################
elseif($mod == 'delete')
{
    if(User::can(DbUsterka::getName(),User::P_DELETE))
    {
        DbUsterka::deleteById($_GET['id']);
        header('Location: zywnosc.php');
    }
}
##############################################
## Select
##############################################
else
{
    if(User::can(DbUsterka::getName(),User::P_SELECT))
    {
        $canUpdate = User::can(DbUsterka::getName(),User::P_UPDATE);
        $canDelete = User::can(DbUsterka::getName(),User::P_DELETE);

        $typy = DbUsterka::selectAll();
        $waznosc_desc = DbUsterka::getWaznoscDescription();
        $status_desc = DbUsterka::getStatusDescription();
        foreach($typy as $element)
        {
            $tpl->setVariablesToUpper($element,true);
            $tpl->setVariable('WAZNOSC_DESC',$waznosc_desc[$element['waznosc']]);
            $tpl->setVariable('STATUS_DESC',$status_desc[$element['status']]);
            $tpl->setVariable('JS_CAN_EDIT',$canUpdate ? 'true' : 'false');
            $tpl->setVariable('JS_CAN_DELETE',$canDelete ? 'true' : 'false');
            $tpl->setVariable('EDYTUJ','');
            $tpl->setVariable('USUN','');

            if($canUpdate)
                $tpl->setVariable('EDYTUJ','<a href="usterka.php?mod=edit&id='.$element['id'].'">Edytuj</a>');
            if($canDelete)
                $tpl->setVariable('USUN','<a href="usterka.php?mod=delete&id='.$element['id'].'" onclick="return confirm(\'Czy na pewno?\')">Usun</a>');

            $tpl->addBlock('BOX_USTERKA_ITEM');
        }
        $tpl->addBlock('BOX_USTERKA');
    }
}
$tpl->generateOutput();