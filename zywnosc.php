<?php
/**
 * Created by PhpStorm.
 * User: Damian
 * Date: 2015-05-24
 * Time: 02:00
 */

require_once('autoLoad.php');

$tpl = new MiniTemplator();
$tpl->readTemplateFromFile('theme/index.html');
User::autoLogin();

if(!User::isLogged())
{
    header('Location: login.php');
}
tpl_showLeftMenu($tpl);
tpl_showHeaderMenu($tpl);

$mod = isset($_GET['mod']) ? $_GET['mod'] : '';
##############################################
## Insert
##############################################
if($mod == 'add')
{
    if(User::can(DbZywnosc::getName(),User::P_INSERT))
    {
        $tpl->setVariable('MOD','addtodb');
        $tpl->setVariable('NAME_FUNCTION','Dodaj żywność');
        $tpl->addBlock('BOX_ZYWNOSC_ADDEDIT');
    }
}
elseif($mod == 'addtodb')
{
    if(User::can(DbZywnosc::getName(),User::P_INSERT))
    {
        $params['nazwa'] = $_POST['nazwa'];
        $params['cena'] = $_POST['cena'];
        DbZywnosc::insert($params);
        header('Location: zywnosc.php');
    }
}
##############################################
## Update
##############################################
elseif($mod == 'edit')
{
    if(User::can(DbZywnosc::getName(),User::P_UPDATE))
    {
        $element = DbZywnosc::selectById($_GET['id']);
        $tpl->setVariablesToUpper($element, true);
        $tpl->setVariable('MOD','edittodb');
        $tpl->setVariable('NAME_FUNCTION','Edytuj żywność');
        $tpl->addBlock('BOX_ZYWNOSC_ADDEDIT');
    }
}
elseif($mod == 'edittodb')
{
    if(User::can(DbZywnosc::getName(),User::P_UPDATE))
    {
        $params['nazwa'] = $_POST['nazwa'];
        $params['cena'] = $_POST['cena'];
        $params['id'] = $_POST['id'];
        DbZywnosc::updateById($params['id'],$params);
        header('Location: zywnosc.php');
    }
}
##############################################
## Delete
##############################################
elseif($mod == 'delete')
{
    if(User::can(DbZywnosc::getName(),User::P_DELETE))
    {
        DbZywnosc::deleteById($_GET['id']);
        header('Location: zywnosc.php');
    }
}
##############################################
## Select
##############################################
else
{
    if(User::can(DbZywnosc::getName(),User::P_SELECT))
    {
        $canUpdate = User::can(DbZywnosc::getName(),User::P_UPDATE);
        $canDelete = User::can(DbZywnosc::getName(),User::P_DELETE);

        $typy = DbZywnosc::selectAll();
        foreach($typy as $element)
        {
            $tpl->setVariablesToUpper($element,true);
            $tpl->setVariable('JS_CAN_EDIT',$canUpdate ? 'true' : 'false');
            $tpl->setVariable('JS_CAN_DELETE',$canDelete ? 'true' : 'false');
            $tpl->setVariable('EDYTUJ','');
            $tpl->setVariable('USUN','');

            if($canUpdate)
                $tpl->setVariable('EDYTUJ','<a href="zywnosc.php?mod=edit&id='.$element['id'].'">Edytuj</a>');
            if($canDelete)
                $tpl->setVariable('USUN','<a href="zywnosc.php?mod=delete&id='.$element['id'].'" onclick="return confirm(\'Czy na pewno?\')">Usun</a>');

            $tpl->addBlock('BOX_ZYWNOSC_ITEM');
        }
        $tpl->addBlock('BOX_ZYWNOSC');
    }
}
$tpl->generateOutput();