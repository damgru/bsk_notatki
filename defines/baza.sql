-- phpMyAdmin SQL Dump
-- version 4.1.12
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 23 Maj 2015, 23:59
-- Server version: 5.6.16
-- PHP Version: 5.5.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `notatki_bsk`
--

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `film`
--

CREATE TABLE IF NOT EXISTS `film` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nazwa` varchar(100) NOT NULL COMMENT 'nazwa filmu',
  `dlugosc` int(11) NOT NULL COMMENT 'długość filmu wyrażona w minutach',
  `data_premiery` date NOT NULL,
  `gatunek` varchar(100) NOT NULL COMMENT 'opisowo',
  `datatime_dodania` datetime NULL,
  PRIMARY KEY (`id`),
  KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='lista filmów w kinie' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `membership`
--

CREATE TABLE IF NOT EXISTS `membership` (
  `id_user` int(11) NOT NULL,
  `id_role` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Zrzut danych tabeli `membership`
--

INSERT INTO `membership` (`id_user`, `id_role`) VALUES
(2, 1),
(9, 6),
(2, 6),
(10, 6),
(11, 9),
(2, 9),
(13, 6),
(2, 10);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `roles`
--

CREATE TABLE IF NOT EXISTS `roles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `time_add` datetime DEFAULT NULL,
  `time_update` datetime DEFAULT NULL,
  `t_users` varchar(4) NOT NULL DEFAULT '0000',
  `t_membership` varchar(4) NOT NULL DEFAULT '0000',
  `t_roles` varchar(4) NOT NULL DEFAULT '0000',
  `t_film` varchar(4) NOT NULL DEFAULT '0000',
  `t_seans` varchar(4) NOT NULL DEFAULT '0000',
  `t_usterka` varchar(4) NOT NULL DEFAULT '0000',
  `t_zywnosc` varchar(4) NOT NULL DEFAULT '0000',
  `t_sprzedaz_zywnosci` varchar(4) NOT NULL DEFAULT '0000',
  `t_typy_biletow` varchar(4) NOT NULL DEFAULT '0000',
  `t_sprzedaz_biletow` varchar(4) NOT NULL DEFAULT '0000',
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`),
  KEY `id` (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=17 ;

--
-- Zrzut danych tabeli `roles`
--

INSERT INTO `roles` (`id`, `name`, `time_add`, `time_update`, `t_users`, `t_membership`, `t_roles`, `t_film`, `t_seans`, `t_usterka`, `t_zywnosc`, `t_sprzedaz_zywnosci`, `t_typy_biletow`, `t_sprzedaz_biletow`) VALUES
(1, 'admin', '2015-04-12 00:00:00', '2015-05-23 23:57:33', '1111', '1011', '1111', '1111', '1111', '1111', '1111', '1111', '1111', '1111'),
(6, 'user', '2015-04-21 12:26:10', '2015-04-29 00:35:08', '0000', '0000', '0000', '0000', '0000', '0000', '0000', '0000', '0000', '0000'),
(9, 'czytac', '2015-04-21 13:19:43', '2015-04-21 13:19:51', '0001', '0000', '0000', '0000', '0000', '0000', '0000', '0000', '0000', '0000'),
(10, 'RoleAdmin', '2015-05-19 22:58:44', '2015-05-19 22:58:54', '0000', '0000', '0000', '0000', '0000', '0000', '0000', '0000', '0000', '0000'),
(12, 'Nazwa roli tab', '2015-05-23 22:51:18', NULL, '1000', '0100', '0001', '0010', '0100', '1111', '0000', '0000', '0000', '0000'),
(16, 'all', '2015-05-23 23:42:41', '2015-05-23 23:52:20', '1010', '1111', '1111', '1111', '0000', '1111', '1111', '0111', '0101', '0111');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `seans`
--

CREATE TABLE IF NOT EXISTS `seans` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nazwa` varchar(100) NOT NULL COMMENT 'nazwa seansu',
  `dlugosc` int(11) NOT NULL COMMENT 'długość seansu w minutach',
  `film_id` int(11) NOT NULL,
  `datatime_seansu` datetime NOT NULL,
  `datatime_dodania` datetime NULL,
  PRIMARY KEY (`id`),
  KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='lista seansów w kinie' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `sprzedaz_biletow`
--

CREATE TABLE IF NOT EXISTS `sprzedaz_biletow` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cena` decimal(15,2) NOT NULL,
  `typy_biletow_id` int(11) NOT NULL,
  `datatime_sprzedazy` datetime NULL,
  `seans_id` int(11) NOT NULL,
  `forma_platnosci` varchar(1) NOT NULL COMMENT 'K-karta,G-gotówka,B-bon,I-inne',
  `user_id_sprzedawca` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `sprzedaz_zywnosci`
--

CREATE TABLE IF NOT EXISTS `sprzedaz_zywnosci` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `zywnosc_id` int(11) NOT NULL,
  `cena` decimal(15,2) NOT NULL,
  `datatime_sprzedazy` datetime NOT NULL,
  `forma_platnosci` varchar(1) NOT NULL COMMENT 'K-karta,G-gotówka,B-bon,I-inne',
  PRIMARY KEY (`id`),
  KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `typy_biletow`
--

CREATE TABLE IF NOT EXISTS `typy_biletow` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nazwa` varchar(100) NOT NULL,
  `cena` decimal(15,2) NOT NULL,
  `status` varchar(1) NOT NULL COMMENT 'A-aktualne, Z-zakonczone',
  `datatime_dodania` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `password` varchar(512) NOT NULL,
  `current_role_id` int(11) DEFAULT NULL,
  `e_mail` varchar(255) NOT NULL,
  `time_add` datetime NOT NULL,
  `time_update` datetime DEFAULT NULL,
  `changePassword` tinyint(1) NOT NULL DEFAULT '1',
  `accountActive` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`),
  KEY `id` (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=13 ;

--
-- Zrzut danych tabeli `users`
--

INSERT INTO `users` (`id`, `name`, `password`, `current_role_id`, `e_mail`, `time_add`, `time_update`, `changePassword`, `accountActive`) VALUES
(2, 'asdf', '$2y$10$H46o9P6MnEEuQozuK6G.AujmI0GwKpSg8R1B6Msm7i/qzZ37KGPhK', 1, 'asdf@ASDF.PL', '2015-04-18 00:00:00', NULL, 1, 1),
(9, 'user', '$2y$10$WXcpcNqXiwFSfbxYI9r5LeaMWtU0AKdLsiSOwCS4aOskkFEuW8Uey', NULL, 'user', '2015-04-21 12:27:19', NULL, 1, 1),
(11, 'piotrs', '$2y$10$Fh09Pt/rwX5e/0W6Q1bq/OUJ8Yc9N4CVAmgu9DjN6Hvek6qhtXPL2', NULL, 'piotrs@eti.pg.gda.pl', '2015-04-21 13:17:37', NULL, 1, 0),
(12, 'cainon', '$2y$10$k5fhaZRs.bjWfn2O6lsbseUf7gAnYTgRBpoPkYspyw9LmbujANLC6', NULL, 'asdf@asfd.pl', '2015-04-29 00:42:17', NULL, 1, 1);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `usterka`
--

CREATE TABLE IF NOT EXISTS `usterka` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `waznosc` varchar(1) NOT NULL COMMENT '1-drobny,2-normalny,3-wazny,4-krytyczny,5-natychmiastowy',
  `status` varchar(1) NOT NULL COMMENT 'N-nowy,O-odrzucony,P-przyjęte,Z-zakonczone',
  `opis` int(11) NOT NULL,
  `datatime_dodania` datetime NULL,
  `datatime_zmiany` datetime DEFAULT NULL,
  `user_id_zglaszajacy` int(11) NOT NULL,
  `user_id_modyfikujacy` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `zywnosc`
--

CREATE TABLE IF NOT EXISTS `zywnosc` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nazwa` varchar(100) NOT NULL,
  `cena` decimal(15,2) NOT NULL,
  `datatime_zmiany` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
