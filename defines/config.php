<?php
/**
 * Created by PhpStorm.
 * User: Damian
 * Date: 2015-04-11
 * Time: 09:19
 */
/* php configuration */
session_start();
error_reporting(E_ALL);

/* Database connection */
define('DB_TYPE','mysql');
define('DB_HOST','localhost');
define('DB_PORT', 3306);
define('DB_USER','root');
define('DB_PASS','');
define('DB_NAME','notatki_bsk');

