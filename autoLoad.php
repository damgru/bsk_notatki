<?php
/**
 * Created by PhpStorm.
 * User: Damian
 * Date: 2015-04-18
 * Time: 23:15
 */

require_once('defines/config.php');

require_once('inc/DgDatabase.php');
$inc = glob("inc/*.php");
foreach ($inc as $filename)
{
    require_once(__DIR__.'/'.$filename);
}

function tpl_showHeaderMenu(MiniTemplator $tpl)
{
    //górna belka menu
    if(User::isLogged())
    {
        $tpl->setVariable('userName', User::getUserName());
        $tpl->addBlock('HEADER_MENU_LOGOUT');

        /*
        $roles = User::getRoles();
        foreach ($roles as $role) {
            if($role['id'] == User::getCurrentRoleId())
            {
                $tpl->setVariable('SELECTED', 'selected');
            }
            else
            {
                $tpl->setVariable('SELECTED', '');
            }
            $tpl->setVariable('ROLE_ID',$role['id']);
            $tpl->setVariable('ROLE_NAME',$role['name']);
            $tpl->addBlock('HEADER_SELECTROLE_ROLE');
        }
        if(count($roles)<=1)
        {
            $tpl->addBlock('HEADER_SELECTROLE_ONLYONE');
        }
        else
        {
            $tpl->addBlock('HEADER_SELECTROLE');
        }*/

    }
    else
    {
        $tpl->addBlock('HEADER_MENU_LOGIN');
        $tpl->addBlock('HEADER_MENU_REGISTER');
    }
    $tpl->addBlock('HEADER_MENU');

    if(isset($_GET['info_error']))
    {
        tpl_showError($tpl,$_GET['info_error']);
    }

    if(isset($_GET['info_ok']))
    {
        tpl_showError($tpl,$_GET['info_ok']);
    }
}

function tpl_showLeftMenu(MiniTemplator $tpl)
{
    //lewa belka menu
    if (User::can(DbUsers::getName(),User::P_SELECT)) $tpl->addBlock('MENU_PROFIL_USERS');
    if (User::can(DbUsers::getName(),User::P_INSERT)) $tpl->addBlock('MENU_ADD_USER');
    if (User::canOneOf(
        array(
            array('users'=>User::P_INSERT),
            array(DbUsers::getName()=>User::P_SELECT))
    )) $tpl->addBlock('MENU_PROFIL'
    );
    //sprzedaz biletow
    if (User::can(DbSprzedazBiletow::getName(),User::P_SELECT)) $tpl->addBlock('MENU_SPRZEDAZBILETOW_LIST');
    if (User::can(DbSprzedazBiletow::getName(),User::P_INSERT)) $tpl->addBlock('MENU_SPRZEDAZBILETOW_ADD');
    if (User::canOneOf(array(array(DbSprzedazBiletow::getName()=>User::P_SELECT),array(DbSprzedazBiletow::getName()=>User::P_INSERT)))) $tpl->addBlock('MENU_SPRZEDAZBILETOW');
    //sprzedaz żywności
    if (User::can(DbSprzedazZywnosci::getName(),User::P_SELECT)) $tpl->addBlock('MENU_SPRZEDAZZYWNOSCI_LIST');
    if (User::can(DbSprzedazZywnosci::getName(),User::P_INSERT)) $tpl->addBlock('MENU_SPRZEDAZZYWNOSCI_ADD');
    if (User::canOneOf(array(array(DbSprzedazZywnosci::getName()=>User::P_SELECT),array(DbSprzedazZywnosci::getName()=>User::P_INSERT)))) $tpl->addBlock('MENU_SPRZEDAZZYWNOSCI');
    //filmy
    if (User::can(DbFilm::getName(),User::P_SELECT)) $tpl->addBlock('MENU_FILM_LIST');
    if (User::can(DbFilm::getName(),User::P_INSERT)) $tpl->addBlock('MENU_FILM_ADD');
    if (User::canOneOf(array(array(DbFilm::getName()=>User::P_SELECT),array(DbFilm::getName()=>User::P_INSERT)))) $tpl->addBlock('MENU_FILM');
    //seans
    if (User::can(DbSeans::getName(),User::P_SELECT)) $tpl->addBlock('MENU_SEANS_LIST');
    if (User::can(DbSeans::getName(),User::P_INSERT)) $tpl->addBlock('MENU_SEANS_ADD');
    if (User::canOneOf(array(array(DbSeans::getName()=>User::P_SELECT),array(DbSeans::getName()=>User::P_INSERT)))) $tpl->addBlock('MENU_SEANS');
    //typybiletow
    if (User::can(DbTypyBiletow::getName(),User::P_SELECT)) $tpl->addBlock('MENU_TYPYBILETOW_LIST');
    if (User::can(DbTypyBiletow::getName(),User::P_INSERT)) $tpl->addBlock('MENU_TYPYBILETOW_ADD');
    if (User::canOneOf(array(array(DbTypyBiletow::getName()=>User::P_SELECT),array(DbTypyBiletow::getName()=>User::P_INSERT)))) $tpl->addBlock('MENU_TYPYBILETOW');
    //żywność
    if (User::can(DbZywnosc::getName(),User::P_SELECT)) $tpl->addBlock('MENU_ZYWNOSC_LIST');
    if (User::can(DbZywnosc::getName(),User::P_INSERT)) $tpl->addBlock('MENU_ZYWNOSC_ADD');
    if (User::canOneOf(array(array(DbZywnosc::getName()=>User::P_SELECT),array(DbZywnosc::getName()=>User::P_INSERT)))) $tpl->addBlock('MENU_ZYWNOSC');
    //usterka
    if (User::can(DbUsterka::getName(),User::P_SELECT)) $tpl->addBlock('MENU_'.'USTERKA'.'_LIST');
    if (User::can(DbUsterka::getName(),User::P_INSERT)) $tpl->addBlock('MENU_'.'USTERKA'.'_ADD');
    if (User::canOneOf(array(array(DbUsterka::getName()=>User::P_SELECT),array(DbUsterka::getName()=>User::P_INSERT)))) $tpl->addBlock('MENU_'.'USTERKA');
    //rbac
    if (User::can(DbRoles::getName(),User::P_SELECT))$tpl->addBlock('MENU_ROLE_LIST');
    if (User::can(DbRoles::getName(),User::P_INSERT))$tpl->addBlock('MENU_ROLE_ADD');
    if (User::canOneOf(array(array(DbMembership::getName()=>User::P_INSERT),array(DbMembership::getName()=>User::P_DELETE)))) $tpl->addBlock('MENU_USERROLE_ADD');
    if (User::canOneOf(array(array(DbRoles::getName()=>User::P_SELECT),array(DbRoles::getName()=>User::P_INSERT),array(DbMembership::getName()=>User::P_INSERT),array(DbMembership::getName()=>User::P_DELETE)))) $tpl->addBlock('MENU_ROLE');
}

function tpl_showError(MiniTemplator $tpl, $message)
{
    $tpl->setVariable('WIADOMOSC_ERROR', $message);
    $tpl->addBlock('ERRORMESSAGE');
}

function tpl_showOkMessage(MiniTemplator $tpl, $message)
{
    $tpl->setVariable('WIADOMOSC_OK', $message);
    $tpl->addBlock('OKMESSAGE');
}

function tpl_showPermissionError(MiniTemplator $tpl, $tableName, $permission_type)
{
    tpl_showError($tpl,'Nie masz uprawnień do ['.$tableName.'::'.User::$p_names[$permission_type].']');

}