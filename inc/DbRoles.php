<?php
/**
 * Created by PhpStorm.
 * User: Damian
 * Date: 2015-05-23
 * Time: 01:01
 */

class DbRoles extends DgDatabase{

    protected static $_tableName = 'roles';
    public static function getName(){
        return self::$_tableName;
    }

    /**
     * @param $id - identyfikator roli
     * @return array
     */
    public static function getRoleByID($id)
    {
        try
        {
            $stmt = self::prepare('SELECT * FROM roles WHERE id=:id');
            $stmt->bindValue(':id',(int)$id,PDO::PARAM_INT);
            $stmt->execute();
            return $stmt->fetch();
        }
        catch(PDOException $e)
        {
            return false;
        }
    }

    public static function getRoleByName($name)
    {
        return self::execute('SELECT * FROM roles WHERE name = ?',array($name))->fetch();
    }

    public static function getTables()
    {
        $q = self::execute("DESCRIBE roles");
        $table = $q->fetchAll();
        unset($table[0]); // id
        unset($table[1]); // name
        unset($table[2]); // time_add
        unset($table[3]); // time_update
        return array_column($table,'Field');
    }

    public static function createRole($param)
    {
        $param['id'] = NULL;
        $param['time_add'] = self::getCurrentDataTime();
        $param['time_update'] = NULL;
        self::_insert(self::$_tableName, $param);
        return self::getPDO()->lastInsertId();
    }

    public static function deleteRoleById($role_id)
    {
        self::getPDO()->beginTransaction();
        self::execute('DELETE FROM membership WHERE id_role = ?', array($role_id));
        self::execute('DELETE FROM roles WHERE id = ?',array($role_id));
        self::getPDO()->commit();
    }

    /**
     * @return array
     */
    public static function getAllRoles()
    {
        $r = self::execute("SELECT * FROM roles");
        return $r->fetchAll();
    }

    /**
     * @param $id
     * @param $params
     */
    public static function editRoleById($id, $params)
    {
        $params['time_update'] = self::getCurrentDataTime();
        $set = array();
        foreach ($params as $key => $value) {
            $set[] = "$key = ?";
            $param[] = $value;
        }
        $sql_set = implode(', ', $set);
        $sql = "UPDATE roles SET $sql_set WHERE id = ?";
        $param[] = $id;

        self::execute($sql,$param);

    }

}