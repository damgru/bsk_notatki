<?php
/**
 * Created by PhpStorm.
 * User: Damian
 * Date: 2015-05-24
 * Time: 01:42
 */

class DbSprzedazBiletow extends DgDatabase{

    protected static $_tableName = 'sprzedaz_biletow';
    public static function getName(){
        return self::$_tableName;
    }

    public static function insert($params)
    {
        $params['datatime_sprzedazy'] = self::getCurrentDataTime();
        return self::_insert(self::$_tableName,$params);
    }

    public static function deleteById($id)
    {
        return self::_deleteById(self::$_tableName,$id);
    }

    public static function updateById($id,$params)
    {
        unset($params['id']); //zeby nie zmieniac
        self::_updateById(self::$_tableName,$id,$params);
    }

    public static function selectAll()
    {
        return self::_selectAll(self::$_tableName);
    }

    public static function selectAllWithSeans()
    {
        $t1 = self::getName();
        $t2 = DbSeans::getName();
        return self::execute("SELECT sb.*, s.nazwa, s.datatime_seansu, s.dlugosc FROM $t1 sb JOIN $t2 s ON sb.seans_id = s.id")->fetchAll();
    }

    public static function selectById($id)
    {
        return self::_selectById(self::$_tableName,$id);
    }

    public static function selectPage($page)
    {
        return self::_selectLimit(self::getName(),$page, 5);
    }

    public static function countAll()
    {
        return self::_count(self::$_tableName);
    }
}