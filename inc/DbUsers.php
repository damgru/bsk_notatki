<?php
/**
 * Created by PhpStorm.
 * User: Damian
 * Date: 2015-05-23
 * Time: 01:39
 */

class DbUsers extends DgDatabase{

    protected static $_tableName = 'users';
    public static function getName(){
        return self::$_tableName;
    }


    /**
     * pobiera wszystkich uzytkownikow
     * @return array
     */
    public static function getUsers()
    {
        $sql = "SELECT * FROM users";
        return self::execute($sql)->fetchAll();
    }

    /**
     * @param string $userName - nazwa użytkownika
     * @param string $password - hasło
     * @param string $email - adres email uzytkownika
     * @return bool - true jezeli zarejestruje, false jezeli wystapi jakis błąd
     * @throws Exception
     */
    public static function registerAccount($userName, $password, $email)
    {
        if(self::accountExists($userName) == false)
        {
            $sql = "INSERT INTO users (id, name, password, e_mail, time_add, time_update) ".
                "VALUES (NULL, ?, ?, ?, ?, NULL)";
            $param[] = $userName;
            $param[] = self::passwordHash($password);
            $param[] = $email;
            $param[] = self::getCurrentDataTime();
            self::execute($sql,$param);
            //$id = self::getPDO()->lastInsertId();
            //self::addUserRoleByName($id, DEFAULT_USER_ROLE);
            return true;
        }

        return false;
    }


    /**
     * @param $id
     * @return mixed|null
     */
    public static function getUserById($id)
    {
        $sql = "SELECT * FROM users WHERE id=?";
        $q = self::execute($sql,array($id));
        if($q->rowCount()>0) {
            return $q->fetch();
        }
        return null;
    }

    /**
     * czy istnieje
     *
     * @param $email
     * @return bool
     */
    public static function emailExists($email)
    {
        $sql = "SELECT id FROM users WHERE e_mail=?";
        $q = self::execute($sql, array($email));
        return ($q->rowCount() > 0);
    }

    /**
     * @param $userName
     * @return mixed|null
     */
    public static function getUserByName($userName)
    {
        $sql = "SELECT * FROM users WHERE name=?";
        $q = self::execute($sql,array($userName));
        if($q->rowCount()>0) {
            return $q->fetch();
        }
        return null;
    }

    public static function editUserNameAndEmailById($userName,$email,$id)
    {
        $sql = "UPDATE users SET name=?, e_mail=? WHERE id=?";
        self::execute($sql,array($userName,$email,$id));
    }

    /**
     * aktywuje konto o określonej nazwie użytkownika
     *
     * @param $username - nazwa użytkownika
     */
    public static function activateAccount($username)
    {
        $sql = "UPDATE users SET accountActive = 1 WHERE name = ?";
        self::execute($sql,array($username));
    }


    /**
     * dezaktywuje konto o określonej nazwie użytkownika
     *
     * @param $username
     */
    public static function deactivateAccount($username)
    {
        $sql = "UPDATE users SET accountActive = 0 WHERE name = ?";
        self::execute($sql,array($username));
    }

    /**
     * usuwa użytkownika o określonym id
     *
     * @param $id - identyfikator użytkownika
     */
    public static function deleteAccountById($id)
    {
        $sql = "DELETE FROM users WHERE id = ?";
        self::execute($sql,array($id));
    }

    /**
     * generuje skrót hasła
     *
     * @param $plainText
     * @return bool|string
     */
    public static function passwordHash($plainText)
    {
        return password_hash($plainText, PASSWORD_DEFAULT);
    }

    /**
     * sprawdza, czy skrót hasła pokrywa się z podanym hasłem
     *
     * @param $plainText
     * @param $hash
     * @return bool
     */
    public static function passwordVerify($plainText,$hash)
    {
        return password_verify($plainText,$hash);
    }

    /**
     * czy istenije konto o określonej nazwie użytkownika
     *
     * @param $userName
     * @return bool
     */
    public static function accountExists($userName)
    {
        $sql = "SELECT id FROM users WHERE name=?";
        $q = self::execute($sql,array($userName));
        return ($q->rowCount()>0);
    }


    public static function editUserEmail($userName, $email)
    {
        $sql = "UPDATE users SET e_mail = ? WHERE name = ?";
        self::execute($sql,array($email,$userName));
    }

    public static function changeUserRole($userName, $currentRoleId)
    {
        $sql = "UPDATE users SET current_role_id = ? WHERE name = ?";
        self::execute($sql,array($currentRoleId,$userName));
    }

    public static function editUserPassword($userName, $newPassword)
    {
        $hashedPassword = self::passwordHash($newPassword);
        $sql = "UPDATE users SET password = ? WHERE name = ?";
        self::execute($sql,array($hashedPassword,$userName));
    }


}