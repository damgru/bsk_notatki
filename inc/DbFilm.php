<?php
/**
 * Created by PhpStorm.
 * User: Damian
 * Date: 2015-05-24
 * Time: 01:42
 */

class DbFilm extends DgDatabase{

    protected static $_tableName = 'film';
    public static function getName(){
        return self::$_tableName;
    }

    public static function insert($params)
    {
        $params['datatime_dodania'] = self::getCurrentDataTime();
        return self::_insert(self::$_tableName,$params);
    }

    public static function deleteById($id)
    {
        return self::_deleteById(self::$_tableName,$id);
    }

    public static function updateById($id,$params)
    {
        unset($params['id']); //zeby nie zmieniac
        self::_updateById(self::$_tableName,$id,$params);
    }

    public static function selectAll()
    {
        return self::_selectAll(self::$_tableName);
    }

    public static function selectById($id)
    {
        return self::_selectById(self::$_tableName,$id);
    }

    public static function selectByName($name)
    {
        $tableName = self::$_tableName;
        return self::execute("SELECT * FROM $tableName WHERE nazwa = ?",array($name))->fetch();
    }
}