<?php
/**
 * Created by PhpStorm.
 * User: Damian
 * Date: 2015-05-31
 * Time: 00:28
 */

class DbSession extends DgDatabase{

    public static function getName()
    {
        return 'session';
    }

    public static function acceptedDatatime()
    {
        return strtotime("-5 minutes");
    }

    public static function selectActiveSessionsByUser($username)
    {
        $t = self::getName();
        $time = date("Y-m-d H:i:s",self::acceptedDatatime());
        $sql = "SELECT * FROM $t WHERE username = ? AND last_used >= ?";
        return self::execute($sql,array($username,$time))->fetchAll();
    }

    public static function addNew($username, $role_id)
    {
        self::deleteById(session_id());
        $params['session_id'] = session_id();
        $params['username'] = $username;
        $params['last_page'] = $_SERVER['REQUEST_URI'];
        $params['role_id'] = $role_id;
        $params['last_used'] = self::getCurrentDataTime();
        $params['browser'] = $_SERVER['HTTP_USER_AGENT'];
        $params['ip'] = $_SERVER['REMOTE_ADDR'];
        self::_insert(self::getName(),$params,false);
    }

    public static function update()
    {
        $where['session_id'] = session_id();
        $params['last_page'] = $_SERVER['REQUEST_URI'];
        $params['last_used'] = self::getCurrentDataTime();
        $params['browser'] = $_SERVER['HTTP_USER_AGENT'];
        self::_update(self::getName(),$params,$where);
    }

    public static function setRole($role_id)
    {
        $t = self::getName();
        $sql = "UPDATE $t SET role_id = ? WHERE session_id = ?";
        $params = array($role_id,session_id());
        self::execute($sql,$params);
    }

    public static function deleteUserSessions($username)
    {
        $t = self::getName();
        $sql = 'DELETE FROM '.$t.' WHERE username = ?';
        self::execute($sql,array($username));
    }

    public static function deleteById($session_id)
    {
        $t = self::getName();
        $sql = 'DELETE FROM '.$t.' WHERE session_id = ?';
        self::execute($sql,array($session_id));
    }

    public static function getCurrentRoleId($username)
    {
        $t = self::getName();
        $sql = "SELECT role_id FROM $t WHERE username = ? LIMIT 1";
        return self::execute($sql,array($username))->fetchColumn();
    }
}