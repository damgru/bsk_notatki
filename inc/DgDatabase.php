<?php
/**
 * Created by PhpStorm.
 * User: Damian
 * Date: 2015-04-19
 * Time: 12:16
 */

class DgDatabase {

    /** @var PDO */
    protected static $pdo;

    /** @var bool */
    protected static $isInited = false;

    /**
     * @return bool true, jezeli sie powiodło, false jezeli nie
     */
    protected static function init()
    {
        if(self::$isInited == false) {
            try {
                // laczenie z baza danych
                self::$pdo = new PDO(DB_TYPE . ':host=' . DB_HOST . ':'.DB_PORT . ';dbname=' . DB_NAME, DB_USER, DB_PASS);
                self::$isInited = true;
                self::execute("SET NAMES latin1");
            } catch (PDOException $e) {
                die("PDO error:".$e->getMessage());
            }
        }
        return self::$isInited;
    }

    /**
     * @param $sql
     * @return PDOStatement
     */
    protected static function prepare($sql)
    {
        self::init();
        return self::$pdo->prepare($sql);
    }

    /**
     * @param $sql - zapytanie sql
     * @param $params - parametry
     * @return PDOStatement
     */
    protected static function execute($sql, $params = null)
    {
        try{
        $q = self::prepare($sql);
        $q->execute($params);
        return $q;
    }
        catch(PDOException $e)
        {
            self::showError($e->getMessage());
        }
        return 0;
    }

    /**
     * @return bool|string
     */
    protected static function getCurrentDataTime()
    {
        return date("Y-m-d H:i:s");
    }

    /**
     * @return PDO
     */
    protected static function getPDO()
    {
        self::init();
        return self::$pdo;
    }

    /**
     * @param $message
     */
    protected static function showError($message){
        trigger_error(self::class.' -> '.$message, E_USER_ERROR);
    }

    /**
     * @param $tableName - nazwa tabeli
     * @param $params ['nazwa_kolumny'] = wartość_kolumny
     * @return PDOStatement
     */
    protected static function _insert($tableName,$params,$autoId = true)
    {
        if($autoId) $params['id'] = null;
        $values = array();
        foreach ($params as $key => $value)
        {
            $values[] = ':' . $key;
        }

        $sql_names = '`' . implode('`, `', array_keys($params)) . '`';
        $sql_values = implode(', ', $values);

        $sql = "INSERT INTO $tableName ($sql_names) VALUES ( $sql_values );";
        $q = self::execute($sql, $params);
        return $q;
    }

    protected static function _selectAll($tableName)
    {
        $sql = "SELECT * FROM $tableName";
        return self::execute($sql)->fetchAll();
    }

    protected static function _selectLimit($tableName, $page, $rowsPerPage = 20)
    {
        if($page<=0) $page = 1;
        $sql = "SELECT * FROM $tableName LIMIT ? OFFSET ?";
        $params = array($rowsPerPage,($page*$rowsPerPage));
        return self::execute($sql,$params)->fetchAll();
    }

    protected static function _countGroupBy($tableName, $groupBy = '')
    {
        $sql_groupby = '';
        if(!empty($groupBy))
        {
            if(!is_array($groupBy))$groupBy = array($groupBy);
            if(count($groupBy)>0)
            {
                $sql_groupby = "GROUP BY ".join(',', $groupBy);
            }

        }
        $sql = "SELECT COUNT(*) FROM $tableName $sql_groupby";
        return self::execute($sql)->fetchAll();
    }

    protected static function _count($tableName)
    {
        $sql = "SELECT COUNT(*) FROM $tableName";
        return self::execute($sql)->fetchColumn();
    }

    protected static function _selectById($tableName, $id)
    {
        $sql = "SELECT * FROM $tableName WHERE id = ?";
        return self::execute($sql,array($id))->fetch();
    }
    protected static function _updateById($tableName,$id,$params)
    {
        $sets = array();
        foreach($params as $key=>$value)
        {
            $sets[] = "$key = ?";
        }

        $sql = "UPDATE $tableName SET ".implode(',',$sets)." WHERE id = ?";
        $values = array_merge(array_values($params),array($id));
        return self::execute($sql,$values);
    }

    protected static function _update($tableName,$params,$wheres)
    {
        $sets = array();
        foreach($params as $key=>$value)
        {
            $sets[] = "$key = ?";
        }

        $where = array();
        foreach($wheres as $key=>$value)
        {
            $where[] = "$key = ?";
        }

        $sql = "UPDATE $tableName SET ".implode(',',$sets)." WHERE ".implode(' AND ',$where);
        $values = array_merge(array_values($params),array_values($wheres));
        return self::execute($sql,$values);
    }

    protected static function _deleteById($tableName,$id)
    {
        $sql = "DELETE FROM $tableName WHERE id = ?";
        return self::execute($sql,array($id));
    }
}