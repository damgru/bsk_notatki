<?php
/**
 * Created by PhpStorm.
 * User: Damian
 * Date: 2015-05-23
 * Time: 02:14
 */

class DbMembership extends DgDatabase{

    protected static $_tableName = 'membership';
    public static function getName(){
        return self::$_tableName;
    }

    public static function getMemberships()
    {
        $sql = 'SELECT membership.* FROM membership';
        $q = self::execute($sql);
        return $q->fetchAll();
    }

    /**
     * @param $userId
     * @param $roleId
     * @return bool
     */
    public static function addUserRole($userId, $roleId)
    {
        $sql = "SELECT COUNT(*) FROM membership WHERE id_user = ? AND id_role = ?";
        $result = self::execute($sql, array($userId,$roleId))->fetch();
        if($result[0] == 0)
        {
            $sql = "INSERT INTO membership (id_user, id_role) VALUES (?, ?)";
            $param[] = $userId;
            $param[] = $roleId;
            self::execute($sql,$param);
            return true;
        }
        else
        {
            return false;
        }
    }

    /**
     * @param $userName
     * @param $roleName
     * @return bool
     */
    public static function addUserRoleByNames($userName, $roleName)
    {
        $user = DbUsers::getUserByName($userName);
        $role = DbRoles::getRoleByName($roleName);
        if(isset($user['id']) and isset($role['id']))
        {
            return self::addUserRole($user['id'],$role['id']);
        }
        else
        {
            return false;
        }

    }


    public static function addUserRoleByName($userId, $roleName)
    {
        $sql = "SELECT id FROM roles WHERE name = ?";
        $q = self::execute($sql, array($roleName));
        $row = $q->fetch();
        self::addUserRole($userId, $row['id']);
    }


    /**
     * @param $userId
     * @param $roleId
     */
    public static function deleteUserRole($userId, $roleId)
    {
        $sql = "DELETE FROM membership WHERE id_user=? AND id_role=?";
        $param[] = $userId;
        $param[] = $roleId;
        self::execute($sql,$param);
    }


    /**
     * Funkcja sprawdza, ilu istnieje użytkowników z danym przywilejem
     * @param $permission_name - nazwa przywileju
     * @return int - liczba użytkowników
     */
    public static function countUsersWithPermission($permission_name)
    {
        $sql = "SELECT count(*) FROM membership LEFT JOIN roles ON membership.id_role = roles.id WHERE {$permission_name} = 1";
        $q = self::execute($sql);
        return (int)$q->fetch()[0];
    }

    public static function getUserRoles($user_id)
    {
        $sql = 'SELECT roles.* FROM membership left join roles on roles.id = membership.id_role where membership.id_user = ?';
        $q = self::execute($sql,array($user_id));
        $roles = array();
        while($row = $q->fetch())
        {
            $roles[] = $row;
        }
        return $roles;
    }

}