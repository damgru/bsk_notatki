<?php
/**
 * Created by PhpStorm.
 * User: Damian
 * Date: 2015-05-24
 * Time: 01:42
 */

class DbUsterka extends DgDatabase{

    protected static $_tableName = 'usterka';
    public static function getName(){
        return self::$_tableName;
    }

    public static function getWaznoscDescription()
    {
        $desc = array(
            1 => 'drobny',
            2 => 'normalny',
            3 => 'wazny',
            4 => 'krytyczny',
            5 => 'natychmiastowy',
        );
        return $desc;
    }

    public static function getStatusDescription()
    {
        $desc = array(
            'N' => 'nowy',
            'O' => 'odrzucony',
            'P' => 'przyjęte',
            'Z' => 'zakończone'
        );
        return $desc;
    }

    public static function insert($params)
    {
        $params['datatime_dodania'] = self::getCurrentDataTime();
        return self::_insert(self::$_tableName,$params);
    }

    public static function deleteById($id)
    {
        return self::_deleteById(self::$_tableName,$id);
    }

    public static function updateById($id,$params)
    {
        unset($params['id']); //zeby nie zmieniac
        $params['datatime_zmiany'] = self::getCurrentDataTime();
        self::_updateById(self::$_tableName,$id,$params);
    }

    public static function selectAll()
    {
        return self::_selectAll(self::$_tableName);
    }

    public static function selectById($id)
    {
        return self::_selectById(self::$_tableName,$id);
    }

    public static function selectByName($name)
    {
        $tableName = self::$_tableName;
        return self::execute("SELECT * FROM $tableName WHERE nazwa = ?",array($name))->fetch();
    }
}