<?php
/**
 * Created by PhpStorm.
 * User: Damian
 * Date: 2015-05-24
 * Time: 01:42
 */

class DbSprzedazZywnosci extends DgDatabase{

    protected static $_tableName = 'sprzedaz_zywnosci';
    public static function getName(){
        return self::$_tableName;
    }

    public static function insert($params)
    {
        $params['datatime_sprzedazy'] = self::getCurrentDataTime();
        return self::_insert(self::$_tableName,$params);
    }

    public static function deleteById($id)
    {
        return self::_deleteById(self::$_tableName,$id);
    }

    public static function updateById($id,$params)
    {
        unset($params['id']); //zeby nie zmieniac
        self::_updateById(self::$_tableName,$id,$params);
    }

    public static function selectAll()
    {
        return self::_selectAll(self::$_tableName);
    }

    public static function selectAllWithZywnosc()
    {
        $t1 = self::getName();
        $t2 = DbZywnosc::getName();
        return self::execute("SELECT sz.*, z.nazwa FROM $t1 sz JOIN $t2 z ON sz.zywnosc_id = z.id")->fetchAll();
    }

    public static function selectById($id)
    {
        return self::_selectById(self::$_tableName,$id);
    }
}