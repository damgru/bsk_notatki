<?php
/**
 * Created by PhpStorm.
 * User: Damian
 * Date: 2015-04-11
 * Time: 22:21
 */

class User {
    /** @var array - tabela roles */
    private static $roles = array();
    /**
     * @var
     */
    private static $db_user;

    /**
     * @var bool
     */
    private static $logged = false;

    public static $p_names = array('SELECT', 'INSERT', 'UPDATE', 'DELETE');
    const P_SELECT = 0; // numeracja wg kolejności w bazie danych
    const P_INSERT = 1;
    const P_UPDATE = 2;
    const P_DELETE = 3;


    const ACCESS_GRANTED = '1';
    const ACCESS_DENIED = '0';


    /**
     *
     */
    public static function autoLogin()
    {
        self::$logged = false;
        if (isset($_SESSION['notatkibsk_id'])) {
            $id = $_SESSION['notatkibsk_id'];
            self::$db_user = DbUsers::getUserById($id);
            if (self::$db_user['id'] == $id and self::$db_user['accountActive'] == 1) {
                self::$logged = true;
                self::loadRoles();
                DbSession::update();
            }
        }
    }

    /**
     * @return bool
     */
    public static function isLogged()
    {
        return self::$logged;
    }

    /**
     *
     */
    public static function loadRoles()
    {
        self::$roles = DbMembership::getUserRoles(self::$db_user['id']);
    }

    public static function getRoles()
    {
        return self::$roles;
    }

    /**
     * @param $login
     * @param $password
     * @return bool - czy zalogowano
     */
    public static function login($login, $password)
    {
        self::$db_user = DbUsers::getUserByName($login);
        if(DbUsers::passwordVerify($password,self::$db_user['password']) and self::$db_user['accountActive'] == 1)
        {
            $_SESSION['notatkibsk_id'] = self::$db_user['id'];
            $_SESSION['notatkibsk_roleid'] = 0;
            self::$logged = true;
            self::loadRoles();
            DbSession::addNew($login,0);
            self::changeRole(null);
            return true;
        }
        else
        {
            self::$logged = false;
            return false;
        }
    }

    /**
     *
     */
    public static function logout()
    {
        self::changeRole(null);
        DbSession::deleteById(session_id());
        $_SESSION['notatkibsk_id'] = '';
        unset($_SESSION['notatkibsk_id']);
        self::$logged = false;
    }

    public static function getUserName()
    {
        return self::$db_user['name'];
    }

    public static function getId()
    {
        return self::$db_user['id'];
    }


    public static function changeRole($roleId)
    {
        foreach(self::$roles as $role)
        {
            if($role['id'] == $roleId or $roleId == null)
            {
                $_SESSION['notatkibsk_roleid'] = $roleId; //wersja z sesją
                //DbUsers::changeUserRole(self::getUserName(), $roleId); //wersja globalna
                DbSession::setRole($roleId);
                break;
            }
        }
    }

    public static function getPossibleRoles()
    {
        $activeSessions = DbSession::selectActiveSessionsByUser(User::getUserName());
        //wyrzucanie jeszzcze niezalogowanych
        foreach($activeSessions as $key=>$s)
        {
            if(empty($s['role_id']))
            {
                unset($activeSessions[$key]);
            }
        }

        if(count($activeSessions)>0)
        {
            foreach($activeSessions as $s)
            {
                if($s['role_id']>0)
                {
                    return array(DbRoles::getRoleByID($s['role_id']));
                }
            }
        }
        return DbMembership::getUserRoles(self::$db_user['id']);
    }

    public static function getCurrentRoleId()
    {
        return $_SESSION['notatkibsk_roleid']; //wersja z sesją
        //return self::$db_user['current_role_id']; //wersja globalna
    }

    public static function getCurrentRole()
    {
        foreach(self::$roles as $role)
        {
            if($role['id'] == self::getCurrentRoleId())
            {
                return $role;
            }
        }
        return false;
    }

    public static function can($tableName, $privilege_type)
    {
        $role = self::getCurrentRole();
        if(!isset($role['t_'.$tableName])){
            return false;
        }
        $pTable = $role['t_'.$tableName];
        if(substr($pTable,$privilege_type,1) == self::ACCESS_GRANTED)
        {
            return true;
        }
        return false;
    }

    public static function canOneOf(array $tableAndType)
    {
        //var_dump($tableAndType);
        foreach($tableAndType as $element)
        {
            $table = key($element);
            $type = $element[$table];
            if(self::can($table,$type))
            {
                //echo 'OK <br>';
                return true;
            }
            //echo 'NOPE <br>';
        }
        return false;
    }

    public static function verifyPassword($password)
    {
        $hash = self::$db_user['password'];
        return DbUsers::passwordVerify($password,$hash);
    }
}