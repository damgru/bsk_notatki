<?php
/**
 * Created by PhpStorm.
 * User: Damian
 * Date: 2015-04-18
 * Time: 23:14
 */

require_once('autoLoad.php');

$tpl = new MiniTemplator();
$tpl->readTemplateFromFile('theme/index.html');

$tpl->addBlock('HEADER_MENU');
$tpl->addBlock('HEADER_MENU_REGISTER');
$tpl->addBlock('HEADER_MENU_LOGIN');
User::autoLogin();

//Logowanie
if(!isset($_GET['mod'])) $_GET['mod'] = '';

if ($_GET['mod'] == 'login') {
    $userName = $_POST['login'];
    $password = $_POST['password'];
    if (User::login($userName, $password)) {
        header('Location: login.php?mod=login2etap');
    }
    else
    {
        header('Location: login.php?info=problemZLogowaniem');
    }
}
//Formularz rejestracji
elseif ($_GET['mod'] == 'register')
{
    $tpl->addBlock('BOX_REGISTER');
} //rejestracja
elseif ($_GET['mod'] == 'submitregister' and User::isLogged()) {
    if(User::can(DbUsers::getName(),User::P_INSERT))
    {
        $userName = $_POST['login'];
        $password = $_POST['password'];
        $password2 = $_POST['password2'];
        $email = $_POST['email'];
        $proceed = true;
        $error = array();


        if (DbUsers::accountExists($userName)) {
            $error[] = 'Takie konto juz istnieje!';
        }
        if (DbUsers::emailExists($email)) {
            $error[] = 'Taki email jest już zarejestrowany!';
        }
        if ($password != $password2) {
            $error[] = 'Hasła się nie zgadzają!';
        }
        if (!isset($_POST['acceptTerms'])) {
            $error[] = 'Musisz zgodzić się na warunki panujace na stronie aby założyć konto.';
        }


        if (empty($error)) {
            DbUsers::registerAccount($userName, $password, $email);
            header('Location: users.php?mod=adduser');

        } else {
            foreach ($error as $e) {
                $tpl->setVariable('WIADOMOSC_ERROR', $e);
                $tpl->addBlock('ERRORMESSAGE');
            }

            $tpl->setVariable('login', $userName);
            $tpl->setVariable('email', $email);
            $tpl->addBlock('BOX_REGISTER');
            tpl_showLeftMenu($tpl);
            tpl_showHeaderMenu($tpl);
        }
    }
    else tpl_showPermissionError($tpl,DbUsers::getName(),User::P_INSERT);

} //wylogowywanie
elseif ($_GET['mod'] == 'logout') {
    User::logout();
    header('Location: index.php?info=wylogowano');
} //formularz logowania
elseif ($_GET['mod'] == 'login2etap')
{
    if(!User::isLogged())
    {
        header('Location: login.php');
    }

    if(isset($_GET['info_error']))
    {
        tpl_showError($tpl,$_GET['info_error']);
    }

    $activeSessions = DbSession::selectActiveSessionsByUser(User::getUserName());

    $roles = User::getPossibleRoles();
    foreach ($roles as $role) {
        if($role['id'] == User::getCurrentRoleId())
        {
            $tpl->setVariable('SELECTED', 'selected');
        }
        else
        {
            $tpl->setVariable('SELECTED', '');
        }
        $tpl->setVariable('ROLE_ID',$role['id']);
        $tpl->setVariable('ROLE_NAME',$role['name']);
        $tpl->addBlock('BOX_SELECTROLE_ROLE');
    }

    if(count($activeSessions)>1)
    {
        foreach($activeSessions as $s)
        {
            $tpl->setVariables($s,true);
            $tpl->addBlock('BOX_SELECTROLE_SESSIONS_ITEM');
        }
        $tpl->addBlock('BOX_SELECTROLE_SESSIONS');
    }

    $tpl->addBlock('BOX_SELECTROLE');

}
elseif ($_GET['mod'] == 'selectrole')
{
    $roles = User::getPossibleRoles();
    //$debug = var_export($roles,true);
    $debug = $_POST['role'];
    foreach($roles as $r)
    {
        if($r['id'] == $_POST['role'])
        {
            $debug .= ','.$r['id'];
            User::changeRole($_POST['role']);
            header('Location: index.php');
            exit();
        }
    }
    header('Location: login.php?mod=login2etap&info_error=Nie możesz wybrać tej roli!');
}
elseif (!User::isLogged())
{
    $tpl->addBlock('BOX_LOGIN');
}

$tpl->generateOutput();