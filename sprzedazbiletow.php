<?php
/**
 * Created by PhpStorm.
 * User: Damian
 * Date: 2015-05-24
 * Time: 02:00
 */

require_once('autoLoad.php');

$tpl = new MiniTemplator();
$tpl->readTemplateFromFile('theme/index.html');
User::autoLogin();

if(!User::isLogged())
{
    header('Location: login.php');
}
tpl_showLeftMenu($tpl);
tpl_showHeaderMenu($tpl);

$mod = isset($_GET['mod']) ? $_GET['mod'] : '';
##############################################
## Insert
##############################################
if($mod == 'add')
{
    if(User::can(DbSprzedazBiletow::getName(),User::P_INSERT) and User::can(DbTypyBiletow::getName(),User::P_SELECT))
    {
        $tpl->setVariable('MOD','addtodb');
        $tpl->setVariable('NAME_FUNCTION','Sprzedaż biletu');

        $typybiletow = DbTypyBiletow::getActiveTypes();
        foreach($typybiletow as $typ)
        {
            $tpl->setVariable('TYP_ID', $typ['id']);
            $tpl->setVariable('TYP_NAZWA', $typ['nazwa']);
            $tpl->setVariable('TYP_CENA',$typ['cena']);
            $tpl->addBlock('BOX_SPRZEDAZBILETOW_ADDEDIT_TYP');
        }

        $seanse = DbSeans::selectAll('ASC');
        {
            foreach($seanse as $seans)
            {
                if(strtotime($seans['datatime_seansu']) >= strtotime('-12 hours'))
                {
                    $tpl->setVariable('SEANS_ID', $seans['id']);
                    $tpl->setVariable('SEANS_NAZWA', $seans['nazwa']);
                    $tpl->setVariable('SEANS_DATATIME_SEANSU', $seans['datatime_seansu']);
                    $tpl->addBlock('BOX_SPRZEDAZBILETOW_ADDEDIT_SEANS');
                }
            }
        }
        $tpl->setVariable('SEANS_ID','');
        $tpl->addBlock('BOX_SPRZEDAZBILETOW_ADDEDIT');
    }
}
elseif($mod == 'addtodb')
{
    if(User::can(DbSprzedazBiletow::getName(),User::P_INSERT))
    {
        $params['forma_platnosci'] = $_POST['forma_platnosci'];
        $params['typy_biletow_id'] = $_POST['typy_biletow_id'];
        $params['seans_id'] = $_POST['seans_id'];
        $params['cena'] = DbTypyBiletow::selectById($params['typy_biletow_id'])['cena'];
        $params['user_id_sprzedawca'] = User::getId();
        DbSprzedazBiletow::insert($params);
        header('Location: sprzedazbiletow.php?mod=add');
    }
}
##############################################
## Update
##############################################
elseif($mod == 'edit')
{
    if(User::can(DbSprzedazBiletow::getName(),User::P_UPDATE))
    {
        $element = DbSprzedazBiletow::selectById($_GET['id']);
        $tpl->setVariablesToUpper($element, true);
        $tpl->setVariable('MOD','edittodb');
        $tpl->setVariable('NAME_FUNCTION','Edytuj sprzedaż');
        $typybiletow = DbTypyBiletow::getActiveTypes();
        foreach($typybiletow as $typ)
        {
            $tpl->setVariable('TYP_ID', $typ['id']);
            $tpl->setVariable('TYP_NAZWA', $typ['nazwa']);
            $tpl->setVariable('TYP_CENA',$typ['cena']);
            $tpl->setVariable('CHECKED_TYP',$typ['id']==$element['typy_biletow_id'] ? 'checked' : '');
            $tpl->addBlock('BOX_SPRZEDAZBILETOW_ADDEDIT_TYP');
        }

        $seans = DbSeans::selectById($element['typy_biletow_id']);
        {
            $tpl->setVariable('SEANS_ID', $seans['id']);
            $tpl->setVariable('SEANS_NAZWA', $seans['nazwa']);
            $tpl->setVariable('SEANS_DATATIME_SEANSU', $seans['datatime_seansu']);
            $tpl->setVariable('CHECKED_SEANS', 'checked');
            $tpl->addBlock('BOX_SPRZEDAZBILETOW_ADDEDIT_SEANS');
        }

        $tpl->setVariable('CHECKED_FP_'.$element['forma_platnosci'], 'checked');

        $tpl->setVariable('SEANS_ID','');
        $tpl->addBlock('BOX_SPRZEDAZBILETOW_ADDEDIT');
    }
}
elseif($mod == 'edittodb')
{
    if(User::can(DbSprzedazBiletow::getName(),User::P_UPDATE))
    {
        $params['forma_platnosci'] = $_POST['forma_platnosci'];
        $params['typy_biletow_id'] = $_POST['typy_biletow_id'];
        $params['seans_id'] = $_POST['seans_id'];
        $params['cena'] = DbTypyBiletow::selectById($params['typy_biletow_id'])['cena'];
        $params['id'] = $_POST['id'];
        DbSprzedazBiletow::updateById($params['id'],$params);
        header('Location: sprzedazbiletow.php');
    }
}
##############################################
## Delete
##############################################
elseif($mod == 'delete')
{
    if(User::can(DbSprzedazBiletow::getName(),User::P_DELETE))
    {
        DbSprzedazBiletow::deleteById($_GET['id']);
        header('Location: sprzedazbiletow.php');
    }
}
##############################################
## Select
##############################################
else
{
    if(User::can(DbSprzedazBiletow::getName(),User::P_SELECT))
    {
        $canUpdate = User::can(DbSprzedazBiletow::getName(),User::P_UPDATE);
        $canDelete = User::can(DbSprzedazBiletow::getName(),User::P_DELETE);

        $typy = DbSprzedazBiletow::selectAllWithSeans();
        foreach($typy as $element)
        {
            $tpl->setVariablesToUpper($element,true);

            $tpl->setVariable('JS_CAN_EDIT',$canUpdate ? 'true' : 'false');
            $tpl->setVariable('JS_CAN_DELETE',$canDelete ? 'true' : 'false');
            $tpl->setVariable('DZISIAJ',date("Y-m-d"),true);
            $tpl->setVariable('SEANS',$element['nazwa']);
            $tpl->setVariable('EDYTUJ','');
            $tpl->setVariable('USUN','');

            if($canUpdate)
                $tpl->setVariable('EDYTUJ','<a href="sprzedazbiletow.php?mod=edit&id='.$element['id'].'">Edytuj</a>');
            if($canDelete)
                $tpl->setVariable('USUN','<a href="sprzedazbiletow.php?mod=delete&id='.$element['id'].'" onclick="return confirm(\'Czy na pewno?\')">Usun</a>');

            $tpl->addBlock('BOX_SPRZEDAZBILETOW_ITEM');
        }
        $tpl->addBlock('BOX_SPRZEDAZBILETOW');
    }
}
$tpl->generateOutput();