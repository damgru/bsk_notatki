# README #
Projekt KINA z wykorzystaniem RBAC + MySQL

# Opis bazy danych #
## TABELE ##
### [roles] ###
* id
* name
* time_add
* time_update
* t_users (varchar 4) [SIUD, 0-brak, 1-moze, np. 1100->SelectInsert, 1001->SelectDelete]
* t_membership
* t_roles
* t_film
* t_seans
* t_usterka
* t_zywnosc
* t_sprzedaz_zywnosci
* t_typy_biletow
* t_sprzedaz_biletow

Najważniejsza tabela, zawiera kolumny o nazwach takich, jak nazwy wszystkich tabel z prefiksem t_. Są one typu (varchar 4) i ich zawartość oznacza [SelectInsertUpdateDelete, 0-brak uprawnien, 1-posiada uprawnienia, np. 1100->SelectInsert, 1001->SelectDelete]

### [users] ###
### [membership] ###
### [film] ###
* id
* nazwa (varchar 100)
* dlugosc (int) [w minutach]
* data_premiery
* gatunek (varchar 100)
* datatime_dodania

### [seans] ###
* id
* nazwa
* dlugosc
* film_id
* datatime_seansu
* datatime_dodania

### [usterka] ###
* id
* waznosc (varchar 1) [1-drobny,2-normalny,3-wazny,4-krytyczny,5-natychmiastowy]
* status (varchar 1) [N-nowy,O-odrzucony,P-przyjęte,Z-zakonczone]
* opis
* datatime_dodania
* datatime_zmiany
* user_id_zglaszajacy
* user_id_modyfikujacy

### [zywnosc] ###
* id
* nazwa (varchar 100)
* cena (decimal 15,2)
* datatime_zmiany

### [sprzedaz_zywnosci] ###
* id
* zywnosc_id
* cena
* datatime_sprzedazy
* forma_platnosci (varchar 1) [K-karta,G-gotówka,B-bon,I-inne]
* user_id_sprzedawca

### [typy_biletow] ###
* id
* nazwa
* cena
* status (varchar 1) [A-aktualne, Z-zakonczone]
* datatime_dodania

### [sprzedaz_biletow] ###
* id
* cena
* typy_biletow_id
* datatime_sprzedazy
* seans_id
* forma_platnosci (varchar 1) [K-karta,G-gotówka,B-bon,I-inne]
* user_id_sprzedawca

# ROLE #
* admin (wszystko)
* techniczny (SUID usterki, S filmy, S seanse)
* kierownik ds. filmów (* filmy, * seanse) 
* hr (IS users)
* sprzedawca biletów (S typy_biletow, I sprzedaz_biletow)
* sprzedawca barowy (S zywnosc, I sprzedaz_zywnosci)
* kierownik (* typy_biletow, * zywnosc, SU sprzedaz_biletow, SU sprzedaz_zywnosci)