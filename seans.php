<?php
/**
 * Created by PhpStorm.
 * User: Damian
 * Date: 2015-05-24
 * Time: 02:00
 */

require_once('autoLoad.php');

$tpl = new MiniTemplator();
$tpl->readTemplateFromFile('theme/index.html');
User::autoLogin();

if(!User::isLogged())
{
    header('Location: login.php');
}
tpl_showLeftMenu($tpl);
tpl_showHeaderMenu($tpl);

$mod = isset($_GET['mod']) ? $_GET['mod'] : '';
##############################################
## Insert
##############################################
if($mod == 'add')
{
    if(User::can(DbSeans::getName(),User::P_INSERT))
    {
        if(User::can(DbFilm::getName(),User::P_SELECT))
        {
            $filmy = DbFilm::selectAll();
            foreach($filmy as $film)
            {
                $js_filmy[] = array('id'=>$film['id'], 'label'=>$film['nazwa']);
            }
        }
        $tpl->setVariable('JS_FILMY',json_encode($js_filmy));

        $tpl->setVariable('MOD','addtodb');
        $tpl->setVariable('NAME_FUNCTION','Dodaj seans');
        $tpl->addBlock('BOX_SEANS_ADDEDIT');
    }
}
elseif($mod == 'addtodb')
{
    if(User::can(DbSeans::getName(),User::P_INSERT))
    {
        $params['nazwa'] = $_POST['nazwa'];
        $params['dlugosc'] = $_POST['dlugosc'];
        $params['datatime_seansu'] = $_POST['datatime_seansu'];
        $params['film_id'] = empty($_POST['film_id']) ? DbFilm::selectByName($_POST['film_name'])['id'] : $_POST['film_id'];

        DbSeans::insert($params);
        header('Location: seans.php');
    }
}
##############################################
## Update
##############################################
elseif($mod == 'edit')
{
    if(User::can(DbSeans::getName(),User::P_UPDATE))
    {
        $seans = DbSeans::selectById($_GET['id']);
        $film_nazwa = DbFilm::selectById($seans['film_id'])['nazwa'];
        $tpl->setVariablesToUpper($seans, true);
        $tpl->setVariable('FILM_NAZWA',$film_nazwa);

        if(User::can(DbFilm::getName(),User::P_SELECT))
        {
            $filmy = DbFilm::selectAll();
            foreach($filmy as $film)
            {
                $js_filmy[] = array('id'=>$film['id'], 'label'=>$film['nazwa']);
            }
        }

        $tpl->setVariable('JS_FILMY',json_encode($js_filmy));

        $tpl->setVariable('MOD','edittodb');
        $tpl->setVariable('NAME_FUNCTION','Edytuj seans');
        $tpl->addBlock('BOX_SEANS_ADDEDIT');
    }
}
elseif($mod == 'edittodb')
{
    if(User::can(DbSeans::getName(),User::P_UPDATE))
    {
        $params['nazwa'] = $_POST['nazwa'];
        $params['dlugosc'] = $_POST['dlugosc'];
        $params['datatime_seansu'] = $_POST['datatime_seansu'];
        $params['film_id'] = empty($_POST['film_id']) ? DbFilm::selectByName($_POST['film_name'])['id'] : $_POST['film_id'];
        $params['id'] = $_POST['id'];
        DbSeans::updateById($params['id'],$params);
        header('Location: seans.php');
    }
}
##############################################
## Delete
##############################################
elseif($mod == 'delete')
{
    if(User::can(DbSeans::getName(),User::P_DELETE))
    {
        DbSeans::deleteById($_GET['id']);
        header('Location: seans.php');
    }
}
##############################################
## Select
##############################################
else
{
    if(User::can(DbSeans::getName(),User::P_SELECT))
    {
        $canUpdateFilm = User::can(DbSeans::getName(),User::P_UPDATE);
        $canDeleteFilm = User::can(DbSeans::getName(),User::P_DELETE);

        $seanse = DbSeans::selectAll();
        foreach($seanse as $seans)
        {
            $tpl->setVariablesToUpper($seans,true);
            $tpl->setVariable('JS_CAN_EDIT',$canUpdateFilm ? 'true' : 'false');
            $tpl->setVariable('JS_CAN_DELETE',$canDeleteFilm ? 'true' : 'false');
            $tpl->setVariable('EDYTUJ','');
            $tpl->setVariable('USUN','');

            if($canUpdateFilm)
                $tpl->setVariable('EDYTUJ','<a href="seans.php?mod=edit&id='.$seans['id'].'">Edytuj</a>');
            if($canDeleteFilm)
                $tpl->setVariable('USUN','<a href="seans.php?mod=delete&id='.$seans['id'].'" onclick="return confirm(\'Czy na pewno?\')">Usun</a>');

            $tpl->addBlock('BOX_SEANS_ITEM');
        }
        $tpl->addBlock('BOX_SEANS');
    }
}
$tpl->generateOutput();