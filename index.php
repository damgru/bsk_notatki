<?php
/**
 * Created by PhpStorm.
 * User: Damian
 * Date: 2015-04-10
 * Time: 22:44
 */

require_once('autoLoad.php');

$tpl = new MiniTemplator();
$tpl->readTemplateFromFile('theme/index.html');
User::autoLogin();

if(!User::isLogged())
{
    header('Location: login.php');
}

tpl_showLeftMenu($tpl);
tpl_showHeaderMenu($tpl);

$tpl->generateOutput();