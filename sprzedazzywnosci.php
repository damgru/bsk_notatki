<?php
/**
 * Created by PhpStorm.
 * User: Damian
 * Date: 2015-05-24
 * Time: 02:00
 */

require_once('autoLoad.php');

$tpl = new MiniTemplator();
$tpl->readTemplateFromFile('theme/index.html');
User::autoLogin();

if(!User::isLogged())
{
    header('Location: login.php');
}
tpl_showLeftMenu($tpl);
tpl_showHeaderMenu($tpl);

$mod = isset($_GET['mod']) ? $_GET['mod'] : '';
##############################################
## Insert
##############################################
if($mod == 'add')
{
    if(User::can(DbSprzedazZywnosci::getName(),User::P_INSERT) and User::can(DbZywnosc::getName(),User::P_SELECT))
    {
        $tpl->setVariable('MOD','addtodb');
        $tpl->setVariable('NAME_FUNCTION','Sprzedaż zywnosci');

        $zywnosc = DbZywnosc::selectAll();
        {
            foreach($zywnosc as $z)
            {
                $tpl->setVariable('ZYWNOSC_ID', $z['id']);
                $tpl->setVariable('ZYWNOSC_NAZWA', $z['nazwa']);
                $tpl->setVariable('ZYWNOSC_CENA', $z['cena']);
                $tpl->addBlock('BOX_SPRZEDAZZYWNOSCI_ADDEDIT_ZYWNOSC');
            }
        }
        $tpl->setVariable('SEANS_ID','');
        $tpl->addBlock('BOX_SPRZEDAZZYWNOSCI_ADDEDIT');
    }
}
elseif($mod == 'addtodb')
{
    if(User::can(DbSprzedazZywnosci::getName(),User::P_INSERT))
    {
        $params['forma_platnosci'] = $_POST['forma_platnosci'];
        $params['zywnosc_id'] = $_POST['zywnosc_id'];
        $params['cena'] = DbZywnosc::selectById($params['zywnosc_id'])['cena'];
        $params['user_id_sprzedawca'] = User::getId();
        DbSprzedazZywnosci::insert($params);
        header('Location: sprzedazzywnosci.php?mod=add');
    }
}
##############################################
## Update
##############################################
elseif($mod == 'edit')
{
    if(User::can(DbSprzedazZywnosci::getName(),User::P_UPDATE))
    {
        $element = DbSprzedazZywnosci::selectById($_GET['id']);
        $tpl->setVariablesToUpper($element, true);
        $tpl->setVariable('MOD','edittodb');
        $tpl->setVariable('NAME_FUNCTION','Edytuj sprzedaż');
        $zywnosc = DbZywnosc::selectAll();
        {
            foreach($zywnosc as $z)
            {
                $tpl->setVariable('ZYWNOSC_ID', $z['id']);
                $tpl->setVariable('ZYWNOSC_NAZWA', $z['nazwa']);
                $tpl->setVariable('ZYWNOSC_CENA', $z['cena']);
                $tpl->setVariable('CHECKED_ZYWNOSC', $z['id'] == $element['zywnosc_id'] ? 'checked' : '');
                $tpl->addBlock('BOX_SPRZEDAZZYWNOSCI_ADDEDIT_ZYWNOSC');
            }
            $tpl->setVariable('CHECKED_FP_'.$element['forma_platnosci'], 'checked');
        }
        $tpl->addBlock('BOX_SPRZEDAZZYWNOSCI_ADDEDIT');
    }
}
elseif($mod == 'edittodb')
{
    if(User::can(DbSprzedazZywnosci::getName(),User::P_UPDATE))
    {
        $params['forma_platnosci'] = $_POST['forma_platnosci'];
        $params['zywnosc_id'] = $_POST['zywnosc_id'];
        $params['cena'] = DbZywnosc::selectById($params['zywnosc_id'])['cena'];
        $params['id'] = $_POST['id'];
        DbSprzedazZywnosci::updateById($params['id'],$params);
        header('Location: sprzedazzywnosci.php');
    }
}
##############################################
## Delete
##############################################
elseif($mod == 'delete')
{
    if(User::can(DbSprzedazZywnosci::getName(),User::P_DELETE))
    {
        DbSprzedazZywnosci::deleteById($_GET['id']);
        header('Location: sprzedazzywnosci.php');
    }
}
##############################################
## Select
##############################################
else
{
    if(User::can(DbSprzedazZywnosci::getName(),User::P_SELECT))
    {
        $canUpdate = User::can(DbSprzedazBiletow::getName(),User::P_UPDATE);
        $canDelete = User::can(DbSprzedazBiletow::getName(),User::P_DELETE);

        $typy = DbSprzedazZywnosci::selectAllWithZywnosc();
        foreach($typy as $element)
        {
            $tpl->setVariablesToUpper($element,true);

            $tpl->setVariable('JS_CAN_EDIT',$canUpdate ? 'true' : 'false');
            $tpl->setVariable('JS_CAN_DELETE',$canDelete ? 'true' : 'false');
            $tpl->setVariable('EDYTUJ','');
            $tpl->setVariable('USUN','');

            if($canUpdate)
                $tpl->setVariable('EDYTUJ','<a href="sprzedazzywnosci.php?mod=edit&id='.$element['id'].'">Edytuj</a>');
            if($canDelete)
                $tpl->setVariable('USUN','<a href="sprzedazzywnosci.php?mod=delete&id='.$element['id'].'" onclick="return confirm(\'Czy na pewno?\')">Usun</a>');

            $tpl->addBlock('BOX_SPRZEDAZZYWNOSCI_ITEM');
        }
        $tpl->addBlock('BOX_SPRZEDAZZYWNOSCI');
    }
}
$tpl->generateOutput();