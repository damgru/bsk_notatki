<?php
/**
 * Created by PhpStorm.
 * User: Damian
 * Date: 2015-05-24
 * Time: 02:00
 */

require_once('autoLoad.php');

$tpl = new MiniTemplator();
$tpl->readTemplateFromFile('theme/index.html');
User::autoLogin();

if(!User::isLogged())
{
    header('Location: login.php');
}
tpl_showLeftMenu($tpl);
tpl_showHeaderMenu($tpl);

$mod = isset($_GET['mod']) ? $_GET['mod'] : '';
##############################################
## Insert
##############################################
if($mod == 'add')
{
    if(User::can(DbFilm::getName(),User::P_INSERT))
    {
        $tpl->setVariable('MOD','addtodb');
        $tpl->setVariable('NAME_FUNCTION','Dodaj film');
        $tpl->addBlock('BOX_FILMY_ADDEDIT');
    }
}
elseif($mod == 'addtodb')
{
    if(User::can(DbFilm::getName(),User::P_INSERT))
    {
        $params['nazwa'] = $_POST['nazwa'];
        $params['dlugosc'] = $_POST['dlugosc'];
        $params['data_premiery'] = $_POST['data_premiery'];
        $params['gatunek'] = $_POST['gatunek'];
        DbFilm::insert($params);
        header('Location: filmy.php');
    }
}
##############################################
## Update
##############################################
elseif($mod == 'edit')
{
    if(User::can(DbFilm::getName(),User::P_UPDATE))
    {
        $film = DbFilm::selectById($_GET['id']);
        $tpl->setVariablesToUpper($film, true);
        $tpl->setVariable('MOD','edittodb');
        $tpl->setVariable('NAME_FUNCTION','Edytuj film');
        $tpl->addBlock('BOX_FILMY_ADDEDIT');
    }
}
elseif($mod == 'edittodb')
{
    if(User::can(DbFilm::getName(),User::P_UPDATE))
    {
        $params['nazwa'] = $_POST['nazwa'];
        $params['dlugosc'] = $_POST['dlugosc'];
        $params['data_premiery'] = $_POST['data_premiery'];
        $params['gatunek'] = $_POST['gatunek'];
        $params['id'] = $_POST['id'];
        DbFilm::updateById($params['id'],$params);
        header('Location: filmy.php');
    }
}
##############################################
## Delete
##############################################
elseif($mod == 'delete')
{
    if(User::can(DbFilm::getName(),User::P_DELETE))
    {
        DbFilm::deleteById($_GET['id']);
        header('Location: filmy.php');
    }
}
##############################################
## Select
##############################################
else
{
    if(User::can(DbFilm::getName(),User::P_SELECT))
    {
        $canUpdateFilm = User::can(DbFilm::getName(),User::P_UPDATE);
        $canDeleteFilm = User::can(DbFilm::getName(),User::P_DELETE);

        $films = DbFilm::selectAll();
        foreach($films as $film)
        {
            $tpl->setVariable('JS_CAN_EDIT',$canUpdateFilm ? 'true' : 'false');
            $tpl->setVariable('JS_CAN_DELETE',$canDeleteFilm ? 'true' : 'false');
            $tpl->setVariable('NAZWA',$film['nazwa']);
            $tpl->setVariable('DATA_PREMIERY',$film['data_premiery']);
            $tpl->setVariable('DLUGOSC',$film['dlugosc']);
            $tpl->setVariable('EDYTUJ','');
            $tpl->setVariable('USUN','');

            if($canUpdateFilm)
                $tpl->setVariable('EDYTUJ','<a href="filmy.php?mod=edit&id='.$film['id'].'">Edytuj</a>');
            if($canDeleteFilm)
                $tpl->setVariable('USUN','<a href="filmy.php?mod=delete&id='.$film['id'].'" onclick="return confirm(\'Czy na pewno?\')">Usun</a>');

            $tpl->addBlock('BOX_FILMY_FILM');
        }
        $tpl->addBlock('BOX_FILMY');
    }
}
$tpl->generateOutput();